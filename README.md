This script reduces the clutter in the
[Einstein Toolkit](https://einsteintoolkit.org) 
[issue tracker](https://bitbucket.org/einsteintoolkit/tickets) by removing some
of the columns that are not often useful and adding extra UI features such as
searching for all tickets that are have something that should be reviewed.

Install from https://greasyfork.org/en/scripts/391296-einstein-toolkit-bitbucket-hide-some-issue-columns